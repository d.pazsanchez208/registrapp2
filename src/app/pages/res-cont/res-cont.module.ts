import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResContPageRoutingModule } from './res-cont-routing.module';

import { ResContPage } from './res-cont.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResContPageRoutingModule
  ],
  declarations: [ResContPage]
})
export class ResContPageModule {}
